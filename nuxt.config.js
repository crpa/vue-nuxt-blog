
module.exports = {
  mode: 'universal',
  env: {
    BASE_URL: process.env.BASE_URL
  },
  /*
  ** Headers of the page
  */
  head: {
    title: '南星',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'keywords', name: 'keywords', content: 'www.nanstar.top, 南星'},
      { hid: 'description', name: 'description', content: '运维笔记' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', type: 'text/css', href: '//at.alicdn.com/t/font_1272831_hw4cotrgvnh.css'} // 新增全局字体样式
    ],
    // script: [
    //   {src: "https://cdn.bootcss.com/showdown/1.9.1/showdown.js"}
    // ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/axios',
  ],
  axios: {
    // baseURL: 'https://cnodejs.org/api/v1',
    baseURL: 'http://127.0.0.1:5000/',
    // or other axios configs.
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  }
}
